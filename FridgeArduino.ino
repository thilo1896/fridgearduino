/* This example shows how to get single-shot range
 measurements from the VL53L0X. The sensor can optionally be
 configured with different ranging profiles, as described in
 the VL53L0X API user manual, to get better performance for
 a certain application. This code is based on the four
 "SingleRanging" examples in the VL53L0X API.

 The range readings are in units of mm. */

#include <Servo.h> 
#include <Wire.h>
#include <VL53L0X.h>

#define MEASUREMENT_SIZE_BALANCE_MEAN 15
#define MEASUREMENT_SIZE_BALANCE_MEDIAN 20
#define MEASUREMENT_SIZE_DISTANCE_MEAN 5
#define MEASUREMENT_SIZE_DISTANCE_MEDIAN 10

Servo myservo;

int analogPin = 0;
int balance_measurements_mean[MEASUREMENT_SIZE_BALANCE_MEAN];
int balance_measurements_median[MEASUREMENT_SIZE_BALANCE_MEDIAN];
float analogMinimum = 149.95;
float twoKilo = 15.76;
float six_six_two_kilo = 460;
float person_weight; // in kg
int balance_value;

VL53L0X sensor;
int sensor_value;
float sensor_height = 2330; // distance between ground and sensor in mm
float person_height; // in cm
int distance_measurements_mean[MEASUREMENT_SIZE_DISTANCE_MEAN];
int distance_measurements_median[MEASUREMENT_SIZE_DISTANCE_MEDIAN];

boolean measurement_finished = false;

// Uncomment this line to use long range mode. This
// increases the sensitivity of the sensor and extends its
// potential range, but increases the likelihood of getting
// an inaccurate reading because of reflections from objects
// other than the intended target. It works best in dark
// conditions.

//#define LONG_RANGE


// Uncomment ONE of these two lines to get
// - higher speed at the cost of lower accuracy OR
// - higher accuracy at the cost of lower speed

//#define HIGH_SPEED
#define HIGH_ACCURACY


void setup()
{
  pinMode(8, INPUT);
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
  pinMode(12, OUTPUT);
  pinMode(13, OUTPUT);
  Serial.begin(9600);
  Wire.setClock(50000);
  Wire.begin();
  Wire.setClock(50000);
  Serial.print("init() = ");
  Serial.println(sensor.init());
  sensor.setTimeout(500);

  myservo.attach(10,1000,2000);
  myservo.writeMicroseconds(1000); //zu
  

#if defined LONG_RANGE
  // lower the return signal rate limit (default is 0.25 MCPS)
  sensor.setSignalRateLimit(0.1);
  // increase laser pulse periods (defaults are 14 and 10 PCLKs)
  sensor.setVcselPulsePeriod(VL53L0X::VcselPeriodPreRange, 18);
  sensor.setVcselPulsePeriod(VL53L0X::VcselPeriodFinalRange, 14);
#endif

#if defined HIGH_SPEED
  // reduce timing budget to 20 ms (default is about 33 ms)
  sensor.setMeasurementTimingBudget(20000);
#elif defined HIGH_ACCURACY
  // increase timing budget to 200 ms
  sensor.setMeasurementTimingBudget(200000);
#endif

digitalWrite(9, HIGH);

digitalWrite(11,HIGH);
digitalWrite(12,HIGH); 
digitalWrite(13,LOW);
}

void loop()
{
  //Serial.println(digitalRead(8));
  if(digitalRead(8) == HIGH){
    myservo.writeMicroseconds(1600); //offen
  } else {
    myservo.writeMicroseconds(1000); //zu
  }
  for (int i = 0; i < 20; i++) {
    int balance_value = analogRead(analogPin);
    balance_measurements_median[i] = balance_value;
    delay(100);
  }
  float median_balance = getMedian(balance_measurements_median, 20);
  person_weight = 66.2 * (median_balance - analogMinimum) / six_six_two_kilo;
    
  if (person_weight > 10) {
    if (!measurement_finished) startMeasurement();
  } else {
    measurement_finished = false;
  }

}

int getMedian(int a[], int size) {
  for (int i = 0; i < (size - 1); i++) {
    for (int j = 0; j < (size - (i+1)); j++) {
      if(a[j] > a[j+1]) {
        uint16_t t = a[j];
        a[j] = a[j+1];
        a[j+1] = t;
      }
    }
  }
  int med = size / 2;
  return a[med];  
}

float getMean(int a[], int size) {
  int sum = 0;
  for (int i = 0; i < size; i++) {
    sum = sum + a[i];
  }
  float mean = sum / float(size);
  return mean;
}

void startMeasurement() {
  delay(500);
   
  sendData(1);
      
  Serial.println("Messung gestartet...");
  for (int i = 0; i < MEASUREMENT_SIZE_BALANCE_MEDIAN; i++) {
    balance_value = analogRead(analogPin);
    balance_measurements_median[i] = balance_value;
    delay(10);
  }
  float median_balance = getMedian(balance_measurements_median, MEASUREMENT_SIZE_BALANCE_MEDIAN);
  person_weight = 66.2 * (median_balance - analogMinimum) / six_six_two_kilo;
    
  for (int i = 0; i < MEASUREMENT_SIZE_DISTANCE_MEDIAN; i++) {
    sensor_value = sensor.readRangeSingleMillimeters();
    distance_measurements_median[i] = sensor_value;
    delay(10);
  }
  person_height = (sensor_height - getMedian(distance_measurements_median, MEASUREMENT_SIZE_DISTANCE_MEDIAN)) / float(10);
  sendToRaspberry(person_weight, person_height);
  measurement_finished = true;
}

void sendToRaspberry(float person_weight, float person_height) {
  int weight = person_weight + 0.5;
  Serial.print(person_weight);
  Serial.println(" kg");
  sendData(byte(weight));
  
  int height = person_height + 7.5;
  Serial.print(height);
  Serial.println(" cm");
  sendData(byte(height));
}

void sendData(byte dataByte){
  digitalWrite(9, LOW);
  delay(20);
  for(int i = 0; i < 8; i++){
     //Serial.print("Sending: ");
     //Serial.println(bitRead(dataByte,i));
     digitalWrite(13,LOW);
     digitalWrite(12,bitRead(dataByte,i));
     delay(20);
     digitalWrite(13,HIGH);
     delay(20);
  }
  
  digitalWrite(9, HIGH);
  delay(5);
  digitalWrite(13,LOW);
}

